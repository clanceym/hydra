# Hydra

Hydra allows you to load multiple tests in parallel that hit a url with a variable id or GUID in it.

## Setup

* Download latest stable PhantomJS from [here](http://phantomjs.org/download.html) and make sure it is in your path.
* Download hydra (TODO: Gemify this)

1. In the data folder create a yml file (following the example) for each test you want to run.
2. Then create a list in the params folder of each config.
3. Add checkers specific to the tests you need to run
4. Run bundle install
5. Run hydra.rb

It will verify the parameters, then run hydra.bat to launch the tests.

## Config file setup
```yaml
---
:base_url: This is the base url, the login url if needed
:login_url: This is the login url if required
:pre_path: This is the part that comes after the base url, before the GUID, can be blank
:post_path: This is the part that comes after the GUID, can be blank
:visibility: Leave this blank to run with PhantomJS
:login_required: Leave this blank if no login is required.
:username_field_id: This is the HTML id for the username field
:password_field_id: This is the HTML id for the password field
:login_button_id:  This is the HTML class for the login button
:username: The username to use
:password: The password to use
:page_checker: The name of the checker file in the lib folder
:page_checker_class: The class of the checker file, note that the method page_checker will be called
:report: The name of the report to output at the end of the run, will be in the report folder
:csv_headers: The headers you would like in your csv report
:ids:
  - array of ids or urls to use
  - this can be a list of urls as well, in which case set the base, pre, and post url to ""
```

##
