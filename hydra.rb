require 'optparse'
require "yaml"

class Hydra
  PARAMS_DIR = File.join(File.dirname(__FILE__), 'lib', "params")

  def self.option_parse(args)
    options = {}

    option_parser = OptionParser.new do |o|
      o.on('-p', '--params [params_file]',
           'What params file should be loaded',
           ) { |params_file| options[:params] = params_file }
      o.on('-v', '--verbose',
           'Turn verbosity off, default is on'
           )  { options[:verbose] = true }
      o.on('-c', '--connections [connections]',
           'How many configs to run, if there are less configs, random will be used'
           ) { |connections| options[:connections] = connections.to_i }
      o.on('-h', '--help', 'Display this screen') do
        puts o
        exit
      end
    end

    begin
      option_parser.parse!
      mandatory = [:params, :connections]
      missing   = mandatory.select{ |param| options[param].nil? }

      unless missing.empty?
        puts "Missing options: #{missing.join(', ')}"
        puts option_parser
        exit
      end
    rescue OptionParser::InvalidOption, OptionParser::MissingArgument
      puts $!.to_s
      puts option_parser
      exit
    end
    options[:connections] ||= 10
    options
  end


  def initialize(config_file)
    @config_file = config_file
    @run_file    = File.join(PARAMS_DIR, config_file)
    @run_list    = YAML.load_file @run_file
    @run_id      = Time.new.to_i
  end

  def load_list
    @configs = @run_list[:configs]
  end

  def print_config
    puts ":#{@run_file}"
    puts @configs.to_yaml
  end

  def write_runner
    File.open("hydra.bat", 'w') do |f|
      f.puts "@ECHO OFF"
      f.puts "if [%1]==[] goto usage"
      f.puts ""
      f.puts "ECHO Starting %1 heads to see %2 pages"
      f.puts "FOR /L %%A IN (1,1,%1) DO ("
      f.puts "  ECHO Head %%A"
      f.puts "  START lib\\head %2 %%A /k"
      f.puts ")"
      f.puts "goto :eof"
      f.puts ""
      f.puts ":usage"
      f.puts "ECHO Usage: hydra_runner H"
      f.puts "ECHO        H = number of heads"
    end
  end

  def run_hydra(number)
    `hydra.bat #{number} #{@config_file}`
  end

end

if __FILE__ == $0
  options = Hydra.option_parse(ARGV)
  puts "**********************************\n#{options.to_yaml}"
  h = Hydra.new(options[:params])
  h.load_list
  h.print_config
  print "If those options look ok, should we proceed with the attack? [y/n]: "
  $stdout.flush

  if STDIN.gets.chomp.upcase == "Y"
    print " and now for some fun!\n"
    # h.write_runner
    h.run_hydra(options[:connections])
  else
    puts "as you wish...\nthat was no fun at all!"
    exit 1
  end
end
