@ECHO OFF
if [%1]==[] goto usage

ECHO Starting %1 heads to see %2 pages
FOR /L %%A IN (1,1,%1) DO (
  ECHO Head %%A
  START lib\head %2 %%A /k
)
goto :eof

:usage
ECHO Usage: hydra_runner H
ECHO        H = number of heads
