class ExampleChecker
  def self.page_checker(browser, id)
    if browser.text.include? "Example Domain"
      title   = browser.h1.text
      info    = browser.link.text
      message = "\bGot to the example page\n"
    else
      message = "\bDidn't get where we were going"
      title   = "No title"
      info    = "No link"
    end
    printf(message)
    return [id, title, info]
  end

  def self.login
  end
end
