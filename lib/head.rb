require 'yaml'
require 'watir-webdriver'
require 'csv'

class Head
  DATA_DIR   = File.join(File.dirname(__FILE__), "data")
  REPORT_DIR = File.join(File.dirname(__FILE__), "report")

  def initialize(input_file, sequence = nil)
    if sequence
      run_list    = YAML.load_file File.join(File.dirname(__FILE__), 'params', input_file)
      if sequence >= run_list[:configs].length
        config_file = run_list[:configs].sample
      else
        config_file = run_list[:configs][sequence.to_i]
      end
    else
      config_file = input_file
    end

    puts "  Using #{config_file}"
    @run_data = YAML.load_file File.join(DATA_DIR, config_file)
    checker   = File.join(File.dirname(__FILE__), @run_data[:page_checker])
    eval("require '#{checker}'")
  end

  def load_ids
    @ids = @run_data[:ids]
  end

  def load_base_url
    @base_url = @run_data[:base_url]
  end

  def load_pre_path
    @pre_path  = @run_data[:pre_path]
  end

  def load_post_path
    @post_path = @run_data[:post_path]
  end

  def create_browser(visible = nil)
    unless visible
      @browser = Watir::Browser.new :phantomjs
    else
      @browser = Watir::Browser.new :chrome, desired_capabilities: Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => {"args" => ["test-type" ]})
    end
  end

  def start_run
    create_browser(@run_data[:visibility])
    login if @run_data[:login_required]
    @browser
  end

  def login
    eval("#{@run_data[:page_checker_class]}.login(@browser, @run_data)")
  end

  def loop_through_ids_and_look_around
    @report = []
    @report << @run_data[:csv_headers]

    @ids.each_with_index do |id, counter|
      print "("
      (@ids.length.to_s.length - counter.to_s.length).to_i.times { print " " }
      print "#{counter}/#{@ids.length}) #{id} - "

      navigate_to_page(id)

      show_wait_spinner{ @report << page_checker(@browser, id) }
    end
  end

  def write_out_report
    output = "#{Time.new.to_i}_#{@run_data[:report]}"
    puts "writing to #{output}"
    File.open(File.join(REPORT_DIR, output), "w") {|f| f.write(@report.inject([]) { |csv, row|  csv << CSV.generate_line(row) }.join(""))} # Write out the summary to a csv file
  end

  def close_the_browser
    @browser.close
  end

  def navigate_to_page(id)
    begin
      @browser.goto("#{@base_url}#{@pre_path}#{id}#{@post_path}")
    rescue
      puts $!, $@
    end
  end

  def page_checker(browser, id)
    eval("#{@run_data[:page_checker_class]}.page_checker(browser, id)")
  end

  def show_wait_spinner(fps = 20)
    chars = %w[| / - \\]
    delay = 1.0/fps
    iter  = 0
    spinner = Thread.new do
      while iter do  # Keep spinning until told otherwise
        print chars[(iter +=1 ) % chars.length]
        sleep delay
        print "\b"
      end
    end
    yield.tap{       # After yielding to the block, save the return value
      iter = false   # Tell the thread to exit, cleaning up after itself…
      spinner.join   # …and wait for it to do so.
    }                # Use the block's return value as the method's
  end

end

# ------------------------------------------
if __FILE__ == $0
  begin
    a0 = ARGV[0]
    puts "a0 = #{a0}"
    a1 = ARGV[1].to_i - 1
    puts "a1 = #{a1}"
    head = Head.new(a0, a1)
    head.load_ids
    head.load_base_url
    head.load_pre_path
    head.load_post_path
    head.start_run
    head.loop_through_ids_and_look_around
    head.write_out_report
  rescue Exception => ex
    puts ex.message
    puts ex.backtrace.join("\n")
    print "     Um, this is embarrassing, what happened?"
    $stdout.flush
    STDIN.gets
  ensure
    head.close_the_browser
  end
  print "How did that look?"
  $stdout.flush
  STDIN.gets
end
