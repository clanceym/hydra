require 'head.rb'

describe Head, "function" do
  before :all do
    @head = Head.new("example_config.yml")
  end

  it "load ids returns a list of ids" do
    expect(@head.load_ids.length).to eq(2)
  end

  it "load base url returns a base url" do
    expect(@head.load_base_url).to eq("http://example.com")
  end

  it "load pre path returns a pre path" do
    expect(@head.load_pre_path).to eq("/module/page?SomeID=")
  end

  it "load post path returns a post path" do
    expect(@head.load_post_path).to be nil
  end

  it "start run returns a browser" do
    expect(@head.start_run.class).to eq(Watir::Browser)
  end

  it "can close the browser" do
    expect(@head.close_the_browser).to be true
  end
end
